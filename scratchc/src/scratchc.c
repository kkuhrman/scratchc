/*
 ============================================================================
 Name        : scratchc.c
 Author      : Karl Kuhrman
 Version     :
 Copyright   : GPL v3
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	puts("Hello World"); /* prints Hello World */
	return EXIT_SUCCESS;
}
